unit Unit1;

interface

uses SysUtils, Astro, Grids, Classes;

procedure ShowerCalc(RA, Decl: extended; const DateTime: TDateTime;
  out Az, Alt: extended);
procedure DeleteARow(StrGr: TStringGrid; ARow: Integer);
procedure ClearTable(StrGr: TStringGrid);
procedure GridSort(StrGrid: TStringGrid; NoColumn: Integer);
procedure GridSortNum(StrGrid: TStringGrid; NoColumn: Integer);
function  Between(CurDate, FDate, LDate: TDateTime): Boolean;
procedure GridSellectRow(StrGrid: TStringGrid; r: Integer);
function GridSerchInCol(StrGrid: TStringGrid; Col: integer; Text: string): integer;

implementation

procedure ShowerCalc(RA, Decl: extended; const DateTime: TDateTime;
  out Az, Alt: extended);
// ��������� ��������� �������� �� ������ LCT (local civile time)
// ���������� ���������� ����������  Longitude, Bias, DaylightBias
var
  Year, Month, Day: word;
  Hour, Min, Sec, mSec: word;
begin
  if (RA = 0) AND (Decl = 0) then
  begin
    Az := 0;
    Alt := 0;
    exit;
  end;
  DecodeDate(DateTime, Year, Month, Day);
  DecodeTime(DateTime, Hour, Min, Sec, mSec);
  LCT := HMSDH(Hour, Min, Sec);
  // test RAHA
  // RA=18h32m21s = 18.53916666
  // LCT=14h36m51.67s = 14.614352777
  // 22.04.1980
  // Longitude=-64
  // Bias=-4
  // DLB=0
  // RAHA(18.53916666, 14.614352777, 22, 4, 1980, -64, -4, 0, HA);
  // HA= 9.873237629       9h52m23.66s
  RAHA(RA / 15, Longitude, LCT, Bias, DaylightBias, CDay, CMonth, CYear, HA);
  // test EQHA
  // HA=5h51m44s = 5.86222
  // Decl=23h13m10s = 23.219444
  // Latitude=52
  // EQHA(5.86222,23.219444,52,Az,Alt);
  // Az=283.2710273   Alt=19.33434522
  EQHA(HA, Decl, Latitude, Az, Alt);
end;

procedure DeleteARow(StrGr: TStringGrid; ARow: Integer);
var
  i, j: Integer;
begin // ������� ������ ������� ��� �����
  with StrGr do
  begin
    for i := ARow + 1 to RowCount - 1 do
      for j := 0 to ColCount - 1 do
        Cells[j, i - 1] := Cells[j, i];
    for i := 0 to ColCount - 1 do
      Cells[i, RowCount - 1] := '';
    RowCount := RowCount - 1;
  end;
end;

procedure ClearTable(StrGr: TStringGrid);
var
  i: Integer;
begin // ������� ������� �������� ���������� StrGr
  with StrGr do
    for i := 1 to RowCount - 1 do
      Rows[i].Clear;
end;

procedure GridSort(StrGrid: TStringGrid; NoColumn: Integer);
var
  Line, PosActual: Integer;
  Row: TStringList;
begin // ���������� �� ���������� ��������� ������� �������
  Row := TStringList.Create;
  for Line := 2 to StrGrid.RowCount - 1 do
  begin
    PosActual := Line;
    Row.Assign(StrGrid.Rows[PosActual]);
    while True do
    begin
      if (PosActual = 1) or (Row.Strings[NoColumn] >= StrGrid.Cells[NoColumn,
        PosActual - 1]) then
        break;
      StrGrid.Rows[PosActual] := StrGrid.Rows[PosActual - 1];
      Dec(PosActual);
    end;
    if (Row.Strings[NoColumn] < StrGrid.Cells[NoColumn, PosActual]) then
      StrGrid.Rows[PosActual].Assign(Row);
  end;
  Row.Free;
end;

procedure GridSortNum(StrGrid: TStringGrid; NoColumn: Integer);
var
  Line, PosActual: Integer;
  Row: TStringList;
begin // ���������� �� �������� ��������� ������� �������
  Row := TStringList.Create;
  for Line := 2 to StrGrid.RowCount - 1 do
  begin
    PosActual := Line;
    Row.Assign(StrGrid.Rows[PosActual]);
    while True do
    begin
      if (PosActual = 1) or (StrToInt(Row.Strings[NoColumn]) >=
        StrToInt(StrGrid.Cells[NoColumn, PosActual - 1])) then
        break;
      StrGrid.Rows[PosActual] := StrGrid.Rows[PosActual - 1];
      Dec(PosActual);
    end;
    if (StrToInt(Row.Strings[NoColumn]) < StrToInt(StrGrid.Cells[NoColumn,
      PosActual])) then
      StrGrid.Rows[PosActual].Assign(Row);
  end;
  Row.Free;
end;

function Between(CurDate, FDate, LDate: TDateTime): Boolean;
begin // �������� �� ���� � ��������
  Result := (CurDate >= FDate) and (CurDate <= LDate);
end;

procedure GridSellectRow(StrGrid: TStringGrid; r: Integer);
begin  // �������� ������ � ������������ �� ��� Grid
  with StrGrid do
  begin
    Row := r;
        Selection := TGridRect(Rect(1, r, 1, r));
        TopRow:= r;
//    Selection := TGridRect(Rect(FixedCols, r, ColCount, r));
  end;
end;

function GridSerchInCol(StrGrid: TStringGrid; Col: integer; Text: string): integer;
var
  i: integer;
begin
  Result:= -1;
  i:= 1;
    while i < StrGrid.RowCount do
    begin
      if StrGrid.Cells[Col,i]= Text then
      begin
        Result:= i;
        break;
      end;
      i:= i+1;
    end;
end;

end.
