unit Unit2;
// Unit ��� ���������� ������ ���� ������
//
interface
uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, ComObj, ActiveX,
  Vcl.ComCtrls, DateUtils, UpdMSbase;
    function CheckExcelInstall:boolean;
    function CheckExcelRun: boolean;
    function RunExcel(DisableAlerts:boolean=true; Visible: boolean=false): boolean;
    function StopExcel:boolean;
    function ActivateSheet(WBIndex:integer; SheetName:string):boolean;
    procedure UpdateShowersData(const Year, FileName: string; out Pass: boolean);
    procedure ExtractShowersInfo(Year: string; out Pass: boolean);
    function CheckExcelFile(FileName, Year: string): boolean;

var

  MyExcel: OleVariant;
  FullFileName: string;
const ExcelApp = 'Excel.Application';
const xlCellTypeLastCell = 11;


implementation

function CheckExcelFile(FileName, Year: string): boolean;
begin
  FullFileName:= ExtractFilePath(Application.ExeName)+ FileName+ '.xlsx';
  if NOT FileExists(FullFileName) then
  begin
    MessageBox(0,'MS Excel sheet not found','Error',MB_OK+MB_ICONERROR);
    Result:= false;
    exit;
  end;
  if NOT RunExcel(true,true) then
    begin
      MessageBox(0,'MS Excel not found','Error',MB_OK+MB_ICONERROR);
      Result:= false;
      exit;
    end;
  MyExcel.Application.WorkBooks.Add(FullFileName);
  try
    MyExcel.WorkSheets.Item[Year].UnProtect;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
      Result:= false;
      StopExcel;
      exit;
    end;
  end;
  Result:= true;
end;

procedure UpdateShowersData(const Year, FileName: string; out Pass: boolean);
var
  S1, SName,q: string;
  LastCol, Col, Row: integer;

Label M1;
begin
  // ������ � Excel ���������� ������� � 1
  // � ������� Excel ������ ��� ������ ������
  S1:=MyExcel.WorkSheets.Item[Year].Cells[3, 1];  // 01.01.Year
// ��������� ��������� ��������������� ������ �� �����
  MyExcel.Cells.SpecialCells(xlCellTypeLastCell).Activate;
// ��������� �������� ������� ���������� ���������
  LastCol := MyExcel.ActiveCell.Column;
  for Row := 3 to 76 do  // last row 30.12.Year
  begin
    S1:= MyExcel.WorkSheets.Item[Year].Cells[Row, 1];
    if  S1= '' then continue;
    Col:= 2;
    Form3.FDConnection1.StartTransaction;
    try
      while Col < LastCol do
        begin
          S1:=MyExcel.WorkSheets.Item[Year].Cells[Row, Col];
          if S1<>'' then
            begin
              with Form3.FDQuery1 do
                begin
                  SQL.Text:='insert into tblShowersData (Date, ShortName, RA, Dec)'+
                    ' VALUES (:Date, :ShortName, :RA, :Dec)';
                  try
                    ParamByName('Date').value:= StrToDateTime(MyExcel.WorkSheets.Item[Year].Cells[Row, 1]);
                    ParamByName('RA').value:= StrToFloat(MyExcel.WorkSheets.Item[Year].Cells[Row, Col]);
                    ParamByName('Dec').value:= StrToFloat(MyExcel.WorkSheets.Item[Year].Cells[Row, Col+1]);
                  except
                    on E: Exception do
                      begin
                        ShowMessage(E.Message);
                        Form3.FDConnection1.Rollback;
                        Form3.StatusBar1.Panels[0].Text := 'Showers data crashe';
                        StopExcel;
                        exit;
                      end;
                  end;
                  SName:=  MyExcel.WorkSheets.Item[Year].Cells[1, Col];
                  ParamByName('ShortName').value:= SName;
                  ExecSQL;
                end;
                if Col < LastCol-2 then Col:= Col+1;
            end;
            Col:= Col+1;
        end;
    Form3.FDConnection1.Commit;
    Pass:= true;
    except
      begin
        Form3.FDConnection1.Rollback;
        Pass:= false;
        MyExcel.WorkSheets.Item[Year].Protect;
        StopExcel;
      end;
    end;
  end;
end;

procedure ExtractShowersInfo(Year: string; out Pass: boolean);
var
  Row, Col, LastCol, i: integer;
  ShortName, FullName, MaxDate, Lambda, V, ZHR, r: string;
  StartDate, EndDate: string;

  Lamb, JD: extended;
  Hour, Min, Sec: extended;
  Day: integer;
begin
  LastCol := MyExcel.ActiveCell.Column;
  Col:= 2;
  Row := 77;
  i:= 1;

  Form3.FDConnection1.StartTransaction;
  try
  while Col < LastCol do
    begin  // ����� ������ ��� � ������� tblShowersInfo
      ShortName:= MyExcel.WorkSheets.Item[Year].Cells[Row, Col];
      FullName:=  MyExcel.WorkSheets.Item[Year].Cells[Row+1, Col];
      MaxDate:= MyExcel.WorkSheets.Item[Year].Cells[Row+2, Col];
      Lambda:= MyExcel.WorkSheets.Item[Year].Cells[Row+3, Col];
      V:= MyExcel.WorkSheets.Item[Year].Cells[Row+4, Col];
      ZHR:= MyExcel.WorkSheets.Item[Year].Cells[Row+5, Col];
      r:=  MyExcel.WorkSheets.Item[Year].Cells[Row+6, Col];
      StartDate:=  MyExcel.WorkSheets.Item[Year].Cells[Row+7, Col];
      EndDate:=  MyExcel.WorkSheets.Item[Year].Cells[Row+8, Col];
      with Form3.FDQuery1 do
        begin
          SQL.Text:='insert into tblShowersInfo (Num, ShortName, FullName, MaxDate, Lambda, V, ZHR, r, StartDate, EndDate)'+
          ' VALUES (:Num, :ShortName, :FullName, :MaxDate, :Lambda, :V, :ZHR, :r, :StartDate, :EndDate)';
          ParamByName('Num').value:= i;
          ParamByName('ShortName').value:= ShortName;
          ParamByName('FullName').value:= FullName;
          ParamByName('MaxDate').value:= StrToDateTime(MaxDate);
          ParamByName('Lambda').value:= Lambda;
          ParamByName('V').value:= V;
          ParamByName('ZHR').value:= ZHR;
          ParamByName('r').value:= r;
          try
          ParamByName('MaxDate').value:= StrToDateTime(MaxDate);
          ParamByName('StartDate').value:= StrToDateTime(StartDate);
          ParamByName('EndDate').value:= StrToDateTime(EndDate);
            except
              on E: Exception do
              begin
                ShowMessage(E.Message);
                Form3.FDConnection1.Rollback;
                Form3.StatusBar1.Panels[0].Text := 'Showers data crashe';
                exit;
              end;
          end;
          ExecSQL;
          Col:= Col+2;
          i:= i+1;
        end;
    end;
    Form3.FDConnection1.Commit;
    Pass:= true;
  except
    Form3.FDConnection1.Rollback;
    Pass:= false;
  end;
  MyExcel.WorkSheets.Item[Year].Protect;
  StopExcel;
end;

function CheckExcelInstall:boolean;
var
  ClassID: TCLSID;
  Rez : HRESULT;
begin
// ���� CLSID OLE-�������
  Rez := CLSIDFromProgID(PWideChar(WideString(ExcelApp)), ClassID);
  if Rez = S_OK then  // ������ ������
    Result := true
  else
    Result := false;
end;

function CheckExcelRun: boolean;
begin
  try
    MyExcel:=GetActiveOleObject(ExcelApp);
    Result:=True;
  except
    Result:=false;
  end;
end;

function RunExcel(DisableAlerts:boolean=true; Visible: boolean=false): boolean;
begin
  try

{��������� ���������� �� Excel}
    if CheckExcelInstall then
      begin
        MyExcel:=CreateOleObject(ExcelApp);
//����������/�� ���������� ��������� ��������� Excel (����� �� ����������)
        MyExcel.Application.EnableEvents:=DisableAlerts;
        MyExcel.Visible:=true;
        Result:=true;
      end
    else
      begin
        MessageBox(0,'MS Excel not install','Error',MB_OK+MB_ICONERROR);
        Result:=false;
      end;
  except
    Result:=false;
  end;
end;

function StopExcel:boolean;
begin
  try
    if MyExcel.Visible then MyExcel.Visible:=false;
    MyExcel.Quit;
    MyExcel:=Unassigned;
    Result:=True;
  except
    Result:=false;
  end;
end;

function ActivateSheet(WBIndex:integer; SheetName:string):boolean;
var i:integer;
begin
  Result:=false;
  try
    if WBIndex>MyExcel.WorkBooks.Count then
      raise Exception.Create('����� �������� ������ ��� WorkBooks. ��������� ����� ��������')
    else
      begin
        for i:=1 to MyExcel.WorkBooks[WBIndex].Sheets.Count do
          if AnsiLowerCase(MyExcel.WorkBooks[WBIndex].Sheets.Item[i].Name)=AnsiLowerCase(SheetName) then
            begin
              MyExcel.WorkBooks[WBIndex].Sheets.Item[i].Activate;
              Result:=true;
              break;
            end;
      end;
  except
    raise Exception.Create('��������� ����� ��������� � �������');
end;
end;

end.
