unit UpdMSbase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, Vcl.StdCtrls, DateUtils, TimeConv,
  Vcl.ComCtrls;

type
  TForm3 = class(TForm)
    FDConnection1: TFDConnection;
    LoTWUserQuery: TFDQuery;
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    FDQuery1: TFDQuery;
    StatusBar1: TStatusBar;
    NewDBConnection: TFDConnection;
    NewDataQuery: TFDQuery;
    Edit1: TEdit;

    procedure Button1Click(Sender: TObject);
    function GetFileDateTime(FileName: string): TDateTime;
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure DateChange(Table, FileDate: string);
    function DateCheck(TableName, FileName: string): Boolean;

  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  Form3: TForm3;
  Path: string;
  F: TextFile;
  Call, StrFileDate,s,q: string;
  FileDate, BaseDate: TDateTime;

implementation

{$R *.dfm}

uses Unit2;

procedure UpdLoTW;

begin
  Form3.StatusBar1.Panels[0].Text:= 'Wait few minut, database update';
  Form3.Button2.Enabled:= False;
  Form3.Button3.Enabled:= False;

  if not Form3.DateCheck('LoTW', 'lotw-user-activity.csv') then
    begin
      Form3.StatusBar1.Panels[0].Text:= 'LoTW base does not require updating';
      Form3.Button2.Enabled:= True;
      Form3.Button3.Enabled:= True;
     exit;
    end;

  Form3.FDConnection1.ExecSQL('DROP TABLE IF EXISTS tblLoTW_User');

  try
    Form3.FDConnection1.ExecSQL('CREATE TABLE  IF NOT EXISTS tblLoTW_User ('+
      'Call varchar(20) DEFAULT NULL,'+
      'DxDateTime datetime NOT NULL DEFAULT "0000-00-00 00:00:00")');
  except
    on E: EDatabaseError do
      ShowMessage('Base error');
  end;

  if FileExists(Path+'lotw-user-activity.csv') then
    begin
      AssignFile(F,Path+'lotw-user-activity.csv');
    try
      try         // Read file
        Reset(F);
        while Not EOF(F) do
          begin
            Readln(F, S);
            Call:= copy(S,1,Pos(',',S)-1);
            S:= copy(S,Pos(',',S)+1,Length(S));  // �������� ���� ���������
            s:= StringReplace(s, ',', ' ',[rfReplaceAll, rfIgnoreCase]);
            With Form3.LoTWUserQuery do
               begin
                 SQL.Text:='insert into tblLoTW_User (Call, DxDateTime) VALUES (:Call, :DxDateTime)';
                 ParamByName('Call').value:=  Call;
                 ParamByName('DxDateTime').value:= s;
                 ExecSQL;
               end;
          end;
      finally
        CloseFile(F);
      end;
    Form3.FDConnection1.Commit;
    except
       Form3.FDConnection1.Rollback;
    end;
  end;

  Form3.DateChange('LoTW', StrFileDate);
  Form3.StatusBar1.Panels[0].Text:= 'LoTW base update';
  Form3.Button2.Enabled:= True;
  Form3.Button3.Enabled:= True;
end;

procedure TForm3.Button2Click(Sender: TObject);
var
  Query: string;
begin
  Button1.Enabled:= False;
  Button3.Enabled:= False;
  Form3.StatusBar1.Panels[0].Text:= 'Wait few minut, database update';
  if not Form3.DateCheck('DxStation', 'call_loc.db3') then
    begin
      Form3.StatusBar1.Panels[0].Text:= 'DxStation base does not require updating';
      Form3.Button2.Enabled:= True;
      Form3.Button3.Enabled:= True;
     exit;
    end;

// Cal Loc base update
  // ������� ������ ������� � ������� �����
  Form3.FDConnection1.ExecSQL('DROP TABLE IF EXISTS tblDxStation');
  Query:= 'CREATE TABLE IF NOT EXISTS tblDxStation ('+
    'DxCallsign varchar(20) NOT NULL,'+
    'DxLocatorID varchar(12) DEFAULT NULL,'+
    'DxDate datetime DEFAULT NULL,'+
    'PRIMARY KEY (DxCallsign));';
  Form3.FDConnection1.ExecSQL(Query);
        // cal_loc db3
  with NewDataQuery do
    begin
      SQL.Clear;
      SQL.Text:= 'SELECT * from tblDxStation';
      Open;
    end;
  Form3.FDConnection1.StartTransaction;
  try
     begin
      NewDataQuery.First;
      while NOT NewDataQuery.Eof do
      begin
        With FDQuery1 do
          begin
            SQL.Text:= 'insert into tblDxStation (DxCallsign, DxLocatorID, DxDate) values (:DxCallsign, :DxLocatorID, :DxDate);';
            Params[0].AsString:= NewDataQuery.FieldByName('DxCallsign').AsString;
            Params[1].AsString:=  NewDataQuery.FieldByName('DxLocatorID').AsString;
            Params[2].AsDateTime:=  NewDataQuery.FieldByName('DxDate').AsDateTime;
            ExecSQL;
          end;
        NewDataQuery.Next;
      end;
    end;
  Form3.FDConnection1.Commit;
  except
    Form3.FDConnection1.Rollback;
    Form3.StatusBar1.Panels[0].Text:= 'Error writing to MySQL base';
  end;
  Button1.Enabled:= True;
  Button3.Enabled:= True;
  Form3.DateChange('DxStation', StrFileDate);
  Form3.StatusBar1.Panels[0].Text:= 'DxStation base update';
end;

procedure TForm3.Button3Click(Sender: TObject);
var
  Pass: boolean;
  Query,Date: string;

begin
  q:= 'select UpdDate from tblUpdDate where BaseTable='+chr(39)+'Showers'+chr(39);
  With Form3.LoTWUserQuery do
    begin
      SQL.Text:=  q;
      Open;
      BaseDate:= FieldByName('UpdDate').AsDateTime;
     end;
  Date:= copy(FormatDate(BaseDate, false),0,4);
  if Form3.Edit1.Text = Date then
  begin
    Form3.StatusBar1.Panels[0].Text:= 'DxStation base does not require updating';
    exit;
  end;


  if not CheckExcelFile('MSOrg', Form3.Edit1.Text)   then
  begin
    Form3.StatusBar1.Panels[0].Text:= 'Invalid year specified';
    exit;
  end;
  Button1.Enabled:= False;
  Button2.Enabled:= False;
  Form3.StatusBar1.Panels[0].Text:= 'Wait few minut, database update';

  Form3.FDConnection1.ExecSQL('DROP TABLE IF EXISTS tblShowersData');
  Query:= 'CREATE TABLE IF NOT EXISTS tblShowersData ('+
    'Date datetime NOT NULL DEFAULT "0000-00-00 00:00:00",'+
    'ShortName varchar(3) DEFAULT NULL,'+
    'RA real DEFAULT NULL,'+
    'Dec real DEFAULT NULL);';
  Form3.FDConnection1.ExecSQL(Query);

  UpdateShowersData(Form3.Edit1.Text, 'MSOrg', Pass);
  Button1.Enabled:= True;
  Button2.Enabled:= True;
  if Pass then Form3.StatusBar1.Panels[0].Text:= 'ShowersData table update'
    else
      begin
        Form3.StatusBar1.Panels[0].Text:= 'ShowersData table update problem';
        exit;
      end;

  Form3.FDConnection1.ExecSQL('DROP TABLE IF EXISTS tblShowersInfo');
  Query:= 'CREATE TABLE IF NOT EXISTS tblShowersInfo ('+
    'Num integer,'+
    'ShortName varchar(3) DEFAULT NULL,'+
    'FullName varchar(30) DEFAULT NULL,'+
    'MaxDate datetime NOT NULL DEFAULT "0000-00-00 00:00:00",'+
    'Lambda varchar(20) DEFAULT NULL,'+
    'V varchar(20) DEFAULT NULL,'+
    'ZHR varchar(20) DEFAULT NULL,'+
    'r varchar(20) DEFAULT NULL,'+
    'StartDate datetime NOT NULL DEFAULT "0000-00-00 00:00:00",'+
    'EndDate datetime NOT NULL DEFAULT "0000-00-00 00:00:00")';
  Form3.FDConnection1.ExecSQL(Query);
  ExtractShowersInfo(Form3.Edit1.Text, Pass);
  if Pass then
    Form3.StatusBar1.Panels[0].Text:= 'All Showers table renew'
  else
    Form3.StatusBar1.Panels[0].Text:= 'ShowersInfo table update problem';
  q:= 'update tblUpdDate set UpdDate='+chr(39)+Form3.Edit1.Text+'-01-01 00:00:00'+chr(39)+' where BaseTable="Showers"';
  With Form3.LoTWUserQuery do
    begin
      SQL.Clear;
      SQL.Text:= q;
      ExecSQL;
    end;
  Button1.Enabled:= true;
  Button2.Enabled:= true;


end;

function TForm3.GetFileDateTime(FileName: string): TDateTime;
var
  intFileAge: LongInt;
begin
  intFileAge := FileAge(FileName);
  if intFileAge = -1 then
    begin
      Result := 0;
      Form3.StatusBar1.Panels[0].Text:= 'File not found';
    end
  else
    Result := FileDateToDateTime(intFileAge)
end;

procedure TForm3.Button1Click(Sender: TObject);
begin
  UpdLoTW;
end;

function TForm3.DateCheck(TableName, FileName: string): Boolean;
var
  q: string;

begin
  // ��������� ����������� ����������
  Result:= True;
  Path:= ExtractFilePath(Application.ExeName);
  FileDate:= RecodeTime(Form3.GetFileDateTime(Path+FileName),0, 0, 0, 0);
  StrFileDate:= FormatDate(FileDate, false);
  q:= 'select UpdDate from tblUpdDate where BaseTable='+chr(39)+TableName+chr(39);
  With Form3.LoTWUserQuery do
    begin
      SQL.Text:=  q;
      Open;
      BaseDate:= FieldByName('UpdDate').AsDateTime;
     end;
  if BaseDate >= FileDate then   Result:= False;
end;

procedure TForm3.DateChange(Table, FileDate: string);
begin
// �������� ���� ���������� ����������
  q:= 'update tblUpdDate set UpdDate='+chr(39)+FileDate+chr(39)+' where BaseTable='+chr(39)+Table+chr(39);
  With Form3.LoTWUserQuery do
    begin
      SQL.Clear;
      SQL.Text:= q;
      ExecSQL;
    end;
end;
end.
