object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'MS Base Update'
  ClientHeight = 223
  ClientWidth = 368
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'LoTW user'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 105
    Width = 343
    Height = 89
    Lines.Strings = (
      #1057#1082#1072#1095#1072#1090#1100' '#1076#1074#1072' '#1092#1072#1081#1083#1072':'
      'LoTW users list   https://lotw.arrl.org/lotw-user-activity.csv'
      'G7RAU call_loc   http://www.g7rau.co.uk/default.aspx?menu=5039')
    TabOrder = 1
  end
  object Button2: TButton
    Left = 8
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Cal/Loc base'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 112
    Top = 8
    Width = 75
    Height = 25
    Caption = 'IMO calendar'
    TabOrder = 3
    OnClick = Button3Click
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 204
    Width = 368
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Edit1: TEdit
    Left = 112
    Top = 48
    Width = 33
    Height = 21
    Hint = 'Enter year to update showers table.'
    NumbersOnly = True
    TabOrder = 5
    Text = '2020'
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=MSOrg.db3'
      'DriverID=SQLite')
    Left = 320
    Top = 8
  end
  object LoTWUserQuery: TFDQuery
    Connection = FDConnection1
    Left = 296
    Top = 56
  end
  object FDQuery1: TFDQuery
    Connection = FDConnection1
    Left = 248
    Top = 56
  end
  object NewDBConnection: TFDConnection
    Params.Strings = (
      'Database=call_loc.db3'
      'DriverID=SQLite')
    Left = 264
    Top = 8
  end
  object NewDataQuery: TFDQuery
    Connection = NewDBConnection
    Left = 216
    Top = 8
  end
end
