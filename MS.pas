unit MS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Math,
  Vcl.ComCtrls, Astro, Geo, INIfiles, DateUtils, Vcl.CheckLst, Vcl.Grids,
  Vcl.Mask, System.Masks,
  IdBaseComponent, IdComponent, IdUDPBase, IdUDPClient, IdSNTP, IdTCPConnection,
  IdTCPClient, IdDayTime, IdTime, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, IdTelnet, IdGlobal;

type
  PMShower = ^TMShower;

  TMShower = record
    FullName: string[25];
    ShortName: string[3];
    StartDate: TDate;
    EndDate: TDate;
    MaxDate: TDateTime;
    ZRH: string[3];
    RA: real;
    Decl: extended;
    Veloc: string[3];
    Active: boolean;
  end;

type
  TForm1 = class(TForm)
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    lbLocator: TLabel;
    edAltitude: TEdit;
    Label6: TLabel;
    Image1: TImage;
    cbActiveShowers: TComboBox;
    tbTime: TTrackBar;
    tbDate: TTrackBar;
    pbIntens: TProgressBar;
    Label3: TLabel;
    lbStart: TLabel;
    lbEnd: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    lbTime: TLabel;
    lbDate: TLabel;
    edAzimuth2: TEdit;
    TrayIcon1: TTrayIcon;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label10: TLabel;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    edAzimuth1: TEdit;
    edCall: TEdit;
    Label5: TLabel;
    lbStation: TLabel;
    Label11: TLabel;
    lbShow: TLabel;
    Label12: TLabel;
    edHLoc: TEdit;
    edKLoc: TEdit;
    edDist: TEdit;
    edAzim: TEdit;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    lbClock: TLabel;
    Label7: TLabel;
    StringGrid1: TStringGrid;
    meHLoc: TMaskEdit;
    IdSNTP1: TIdSNTP;
    cbTSync: TCheckBox;
    cmbSNTP: TComboBox;
    Label9: TLabel;
    DateTimePicker1: TDateTimePicker;
    meLoc: TMaskEdit;
    edHSLoc: TEdit;
    btnDelRow: TButton;
    meTime: TMaskEdit;
    Label17: TLabel;
    edElev: TEdit;
    Memo1: TMemo;
    meCall: TMaskEdit;
    FDConnection1: TFDConnection;
    FDQuery1: TFDQuery;
    rgAzIndic: TRadioGroup;
    TabSheet5: TTabSheet;
    btnConn: TButton;
    Memo2: TMemo;
    IdTelnet1: TIdTelnet;
    edCommand: TEdit;
    rgChat: TRadioGroup;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    edKSTpassw: TEdit;
    StringGrid2: TStringGrid;
    btnUser: TButton;
    Timer2: TTimer;
    cbFreeze: TCheckBox;
    cmbPerSync: TComboBox;
    Timer3: TTimer;
    Label21: TLabel;
    Label22: TLabel;
    cbSelect: TCheckBox;
    Panel1: TPanel;
    Label8: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    edQTFdelta: TEdit;
    Label26: TLabel;
    edQRBmin: TEdit;
    edQRBmax: TEdit;
    LoTWUserQuery: TFDQuery;
    cbUsKST: TCheckBox;
    Label23: TLabel;
    lbMax: TLabel;
    StringGrid3: TStringGrid;
    Label1: TLabel;
    Label27: TLabel;
    lbZHR: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    lbV: TLabel;

    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure DrawSky;
    procedure cbActiveShowersSelect(Sender: TObject);
    procedure tbTimeChange(Sender: TObject);
    procedure Intensity;
    procedure tbDateChange(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure ReadShowers;
    procedure edCallKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Radiant(Sname: string);
    procedure TabSheet3Show(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure ViewAzDist;
    procedure SaveNotes;
    procedure LoadNotes;
    procedure TimeSync;
    procedure btnDelRowClick(Sender: TObject);
    procedure cbTSyncClick(Sender: TObject);
    procedure StringGrid1MouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: boolean);
    procedure meHLocKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edHSLocEnter(Sender: TObject);
    procedure meCallKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure meLocKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DateTimePicker1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edCallMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TabSheet4Show(Sender: TObject);
    procedure StringGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure meTimeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnConnClick(Sender: TObject);
    procedure IdTelnet1Connected(Sender: TObject);
    procedure IdTelnet1Disconnected(Sender: TObject);
    procedure IdTelnet1DataAvailable(Sender: TIdTelnet; const Buffer: TIdBytes);
    procedure edCommandKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SendCommand(CommStr: string);
    procedure btnUserClick(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure StringGrid2MouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure UserList;
    procedure ReadSquareList;
    procedure rgChatClick(Sender: TObject);
    procedure StringGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure StringGrid2MouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure Timer3Timer(Sender: TObject);
    procedure cmbSNTPChange(Sender: TObject);
    procedure cmbPerSyncChange(Sender: TObject);
    procedure cbSelectMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edQTFdeltaChange(Sender: TObject);
    procedure edQRBminChange(Sender: TObject);
    procedure edQRBmaxChange(Sender: TObject);
    procedure SelectCondition;
    procedure StringGrid3SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GetShowerPar(SName: string; Date: TDateTime);
    procedure StringGrid3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGrid3MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);

  private
    { Private declarations }
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  CLCTDate: TDateTime;   // Current date, Local Time
  CUTCDate: TDateTime;   // Current date, Universal Time
  FDate, LDate, MDate: TDateTime; // First, last & maximum date of selected shower
  IniFile: TIniFile;
  Path: string;
  Loc, Call: string;
  SName: string;         // Short name selected in Work Tab shower
  STMName: string;         // Short name selected in TM Tab shower
  NodeData: PMShower;
  NewNode, FNode, CNode: TTreeNode;
  ACol, ARow: Integer;
  Logged: Integer;
  Answ: TStringList;
  Ready: boolean;
  ans: string;
  wkd, cfm: TStringList;
  TMCDate: TDateTime;

const
  R = 260.0; // Radius

implementation

{$R *.dfm}

uses TimeConv, Unit1;

procedure TForm1.cmbPerSyncChange(Sender: TObject);
begin // ������������� ������ � ��������� ������������� �������
  case cmbPerSync.ItemIndex of
    0:
      Timer3.Interval := 300000; // 5
    1:
      Timer3.Interval := 900000; // 15
    2:
      Timer3.Interval := 1800000; // 30
    3:
      Timer3.Interval := 3600000; // 60
    4:
      Timer3.Interval := 7200000; // 120
  end;
  TimeSync;
end;

procedure TForm1.cmbSNTPChange(Sender: TObject);
begin
  TimeSync; // ��� ��������� ������� ����� ����������������
end;

procedure TForm1.TabSheet1Show(Sender: TObject);
begin
  // Tab Work
  if (length(StringGrid1.Cells[StringGrid1.Col + 1, StringGrid1.Row]) = 6) then
  begin // ���� � ������� ������ ���� �������, �� ���������
    edKLoc.Text := StringGrid1.Cells[StringGrid1.Col + 1, StringGrid1.Row];
    ViewAzDist;
  end;
  cbActiveShowersSelect(Self);
end;

procedure TForm1.TabSheet2Show(Sender: TObject);
begin
  // Tab Shower Info
  DrawSky;
end;

procedure TForm1.TabSheet3Show(Sender: TObject);
// Tab TM
begin
  DrawSky;
  if (tbDate.Position=0)AND(tbTime.Position=0) then
    begin
    if Between(CUTCDate, FDate, LDate) then
      begin
        tbTime.Position := Round(HourOf(CUTCDate) + MinuteOf(CUTCDate) / 60);
        tbDate.Position := DaysBetween(FDate, CUTCDate);
      end
    else
      begin
        tbTime.Position := 0;
        tbDate.Position := 0;
      end;
    end;
  tbDateChange(nil);
end;

procedure TForm1.TabSheet4Show(Sender: TObject);
begin // ���� ���� ����� �������� ������
  if edHSLoc.Text = '' then
    StatusBar1.Panels[0].Text := 'Write home QRA locator & press Enter';
end;

procedure TForm1.tbDateChange(Sender: TObject);
begin // ��������� ���� �� ������ ������ �� ���-�� ���� �������� �������
  TMCDate := IncDay(FDate, tbDate.Position);
  lbDate.Caption := DateToStr(TMCDate);
  tbTimeChange(nil);
end;

procedure TForm1.tbTimeChange(Sender: TObject);
var
  Hour, Min: Word;
begin // ���������� ����� �������� �������
  Hour := tbTime.Position;
  Min := 0;
  TMCDate := RecodeHour(TMCDate, Hour);
  TMCDate := RecodeMinute(TMCDate, Min);
  lbTime.Caption := IntToStr(Hour) + ':00:00 UTC';
  GetShowerPar(STMName, TMCDate);
  ShowerCalc(RA, Decl, UTCToLocalTime(TMCDate), Az, Alt);
  DrawSky;
  Radiant(STMName);
  Intensity;
end;

procedure TForm1.TimeSync;
begin // ������������� ������� �� �������
  if NOT cbTSync.Checked then
    exit;
  try
    IdSNTP1.Host := cmbSNTP.Items[cmbSNTP.ItemIndex];
    if IdSNTP1.SyncTime then
      Label9.Caption := 'Time sync at ' + DateTimeToStr(LocalTimeToUTC(Now));
  except
    on E: Exception do
      ShowMessage('Time sync ' + E.Message);
  end;
  IdSNTP1.Disconnect;
end;

procedure TForm1.ViewAzDist;
// ��������� � ���������� ���������, ������, ��������
var
  Dist, Azim, Elev, dAz: extended;
begin
  MSDistAzim(edHLoc.Text, edKLoc.Text, Dist, Azim, Elev, dAz);
  edDist.Text := FloatToStr(Round(Dist)) + ' km';
  edAzim.Text := FloatToStr(Round(Azim)) + ' deg';
  edElev.Text := FloatToStr(Round(Elev)) + ' deg';
end;

procedure TForm1.btnConnClick(Sender: TObject);
begin // Connect / Disconnect
  if (btnConn.Caption = 'Connect') then
  begin
    if edKSTpassw.Text = '' then
    begin
      StatusBar1.Panels[0].Text := 'Enter KST chat password on Setup tab';
      exit
    end;
    StatusBar1.Panels[0].Text := '';
    Logged := 0;
    IdTelnet1.Host := 'www.on4kst.org';
    IdTelnet1.port := 23000;
    IdTelnet1.Connect;
    ClearTable(StringGrid2);
    StringGrid2.RowCount := 2;
    rgChat.Enabled := false;
  end;
  if (btnConn.Caption = 'Disconnect') AND (Logged = 2) then
  begin
    SendCommand('/Q');
    sleep(2000);
    IdTelnet1.Disconnect(false);
    Logged := 0;
    ClearTable(StringGrid2);
    StringGrid2.RowCount := 2;
    rgChat.Enabled := true;
    cbSelect.Checked := false;
    cbSelect.Visible := false;
  end;
end;

procedure TForm1.btnUserClick(Sender: TObject);
begin // �������� ������ ������
  if Logged <> 2 then
    exit;
  // SendCommand('/UNSET DX');
  // sleep(1000);
  // Ready:= false;
  SendCommand('/SH US');
  Logged := 3;
  ans := '';
  ClearTable(StringGrid2);
  StringGrid2.RowCount := 2;
  cbSelect.Visible := true;
  // cbSelect.Checked:= false;
end;

procedure TForm1.btnDelRowClick(Sender: TObject);
begin // ������� ������ � �������
  DeleteARow(StringGrid1, StringGrid1.Row);
  StringGrid1.Col := 0;
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin // ���������� ���������� ��� �������� �����
  StatusBar1.Panels[0].Text := 'Disconnect chat server';
  IniFile.WriteInteger('Form', 'FormTop', Form1.Top);
  IniFile.WriteInteger('Form', 'FormLeft', Form1.Left);
  IniFile.WriteInteger('Form', 'FormHeight', Form1.Height);
  IniFile.WriteInteger('Form', 'FormWidth', Form1.Width);
  IniFile.WriteInteger('Form', 'Col0', StringGrid1.ColWidths[0]);
  IniFile.WriteInteger('Form', 'Col1', StringGrid1.ColWidths[1]);
  IniFile.WriteInteger('Form', 'Col2', StringGrid1.ColWidths[2]);
  IniFile.WriteInteger('Form', 'Col3', StringGrid1.ColWidths[3]);
  IniFile.WriteInteger('Form', 'Col4', StringGrid1.ColWidths[4]);
  IniFile.WriteInteger('Form', '2Col0', StringGrid2.ColWidths[0]);
  IniFile.WriteInteger('Form', '2Col1', StringGrid2.ColWidths[1]);
  IniFile.WriteInteger('Form', '2Col2', StringGrid2.ColWidths[2]);
  IniFile.WriteInteger('Form', '2Col3', StringGrid2.ColWidths[3]);
  IniFile.WriteInteger('Form', '2Col4', StringGrid2.ColWidths[4]);
  IniFile.WriteInteger('Form', '3Col0', StringGrid3.ColWidths[0]);
  IniFile.WriteInteger('Form', '3Col1', StringGrid3.ColWidths[1]);
  IniFile.WriteInteger('Form', '3Col2', StringGrid3.ColWidths[2]);
  IniFile.WriteInteger('Form', '3Col3', StringGrid3.ColWidths[3]);
  IniFile.WriteInteger('Form', '3Col4', StringGrid3.ColWidths[4]);
  SaveNotes;
  IniFile.WriteString('Conf', 'Loc', Loc);
  IniFile.WriteString('Conf', 'Call', Call);
  IniFile.WriteBool('Conf', 'Tsync', cbTSync.Checked);
  IniFile.WriteInteger('Conf', 'SERV', cmbSNTP.ItemIndex);
  IniFile.WriteInteger('Conf', 'PSYNC', cmbPerSync.ItemIndex);
  IniFile.WriteBool('Conf', 'KST', cbUsKST.Checked);
  IniFile.WriteString('Conf', 'KSTpassw', edKSTpassw.Text);
  IniFile.WriteInteger('Conf', 'AzIndic', rgAzIndic.ItemIndex);
  IniFile.WriteInteger('Conf', 'Chat', rgChat.ItemIndex);
  IniFile.WriteString('Conf', 'QTFdelta', edQTFdelta.Text);
  IniFile.WriteString('Conf', 'QRBmin', edQRBmin.Text);
  IniFile.WriteString('Conf', 'QRBmax', edQRBmax.Text);
  IniFile.Free;
end;

procedure TForm1.SaveNotes;
var
  f: textfile;
  i, j: Integer;
begin // ���������� ����� �������
  assignfile(f, 'Notes.txt');
  rewrite(f);
  For j := 1 to StringGrid1.RowCount - 1 do
    For i := 0 to StringGrid1.ColCount - 1 do
      writeln(f, StringGrid1.Cells[i, j]);
  closefile(f);
end;

procedure TForm1.LoadNotes;
var
  f: textfile;
  j, i: Integer;
  tempstr: string;
begin // �������� ����� �������
  if NOT FileExists('Notes.txt') then
    exit;
  assignfile(f, 'Notes.txt');
  reset(f);
  j := 0;
  while not Eof(f) do
  begin
    j := j + 1; // rows
    with StringGrid1 do
    begin
      For i := 0 to StringGrid1.ColCount - 1 do
      begin
        readln(f, tempstr);
        StringGrid1.Cells[i, j] := tempstr;
      end;
      RowCount := StringGrid1.RowCount + 1;
    end;
  end;
  StringGrid1.RowCount := StringGrid1.RowCount - 1;
  closefile(f);
end;

procedure TForm1.edHSLocEnter(Sender: TObject);
begin // ���� ��������� ��������
  if length(edHSLoc.Text) = 6 then
    meHLoc.Text := edHSLoc.Text;
  edHSLoc.Visible := false;
  meHLoc.Visible := true;
  meHLoc.SetFocus;
  StatusBar1.Panels[0].Text := 'Write home QRA locator & press Enter';
end;

procedure TForm1.edQRBmaxChange(Sender: TObject);
begin
  btnUserClick(Self);
  if cbSelect.Checked then
    SelectCondition;
end;

procedure TForm1.edQRBminChange(Sender: TObject);
begin
  btnUserClick(Self);
  if cbSelect.Checked then
    SelectCondition;
end;

procedure TForm1.edQTFdeltaChange(Sender: TObject);
begin
  btnUserClick(Self);
  if cbSelect.Checked then
    SelectCondition;
end;

procedure TForm1.meCallKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin // ���� ��������� � ����������� �������� �� ����
  If (Key = 13) then
  begin
    StringGrid1.Enabled := true;
    meCall.Text := UpperCase(meCall.Text);
    StringGrid1.Cells[StringGrid1.Col, StringGrid1.Row] := meCall.Text;
    meCall.Visible := false;
    StringGrid1.SetFocus;
    StatusBar1.Panels[0].Text := '';
    if StringGrid1.Cells[1, StringGrid1.Row] = '' then
    begin // � ����������� �������� �� ���� ���� ������ ������
      try
        with FDQuery1 do
        begin
          SQL.Clear;
          SQL.Text :=
            'select DxCallsign, DxLocatorID from tblDxStation where DxCallsign= '
            + chr(39) + meCall.Text + chr(39);
          Open;
        end;
      except
        on E: Exception do
        begin
          ShowMessage('FDQuery ' + E.Message);
        end;
      end;
      StringGrid1.Cells[StringGrid1.Col + 1, StringGrid1.Row] :=
        FDQuery1.FieldByName('DxLocatorID').AsString;
      edKLoc.Text := StringGrid1.Cells[StringGrid1.Col + 1, StringGrid1.Row];
    end;
    meCall.Text := '';
    ViewAzDist; // ��������� ��������� � ��.
  end;
end;

procedure TForm1.meHLocKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin // ���� ��������� ��������
  if CheckLocator(meHLoc.Text) then
  begin
    Loc := meHLoc.Text;
    lbLocator.Caption := Loc;
    edHSLoc.Text := Loc;
    edHLoc.Text := Loc;
    Coordinate(Loc, Latitude, Longitude);
    meHLoc.Visible := false;
    edHSLoc.Visible := true;
    StatusBar1.Panels[0].Text := '';
  end;
end;

procedure TForm1.meLocKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin // �������� ��������
  If (Key = 13) then
  begin
    if CheckLocator(meLoc.Text) then
    begin
      StringGrid1.Enabled := true;
      StringGrid1.Cells[StringGrid1.Col, StringGrid1.Row] := meLoc.Text;
      meLoc.Visible := false;
      StringGrid1.Enabled := true;
      StringGrid1.SetFocus;
      StatusBar1.Panels[0].Text := '';
      edKLoc.Text := meLoc.Text;
      ViewAzDist;
    end
    else
    begin
      meLoc.SelectAll;
      StatusBar1.Panels[0].Text := 'Bad QRA locator';
    end;
  end;
end;

procedure TForm1.meTimeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin // �������� ������������ ��������� �������
  if (Key = 13) OR (Key = 9) then
  begin
    StringGrid1.Enabled := true;
    StringGrid1.Cells[StringGrid1.Col, StringGrid1.Row] := meTime.Text;
    meTime.Visible := false;
    meTime.Text := ':';
    StringGrid1.Enabled := true;
    StringGrid1.SetFocus;
    StatusBar1.Panels[0].Text := '';
  end;
  case pos(' ', meTime.Text) of
    2:
      if StrToInt(copy(meTime.Text, 1, 1)) > 2 then
        meTime.Text := '  :  ';
    4:
      if StrToInt(copy(meTime.Text, 1, 2)) > 23 then
        meTime.Text := '  :  ';
    5:
      if StrToInt(copy(meTime.Text, 4, 1)) > 5 then
        meTime.Text := '  :  ';
  end;
end;

procedure TForm1.IdTelnet1Connected(Sender: TObject);
begin
  btnConn.Caption := 'Disconnect';
end;

procedure TForm1.IdTelnet1DataAvailable(Sender: TIdTelnet;
  const Buffer: TIdBytes);
const
  CR = #13;
  LF = #10;
var
  Start, Stop: Integer;
  s: string;
Label m1;
begin
  s := BytesToStringRaw(Buffer); // �������������� ���� � ������
  Ready := true;
  if Logged > 1 then
    goto m1;
  if Logged = 0 then
  begin
    if pos('Login:', s) <> 0 then
    begin
      SendCommand(edCall.Text);
      Logged := 1;
    end;
  end;
  if (Logged = 1) AND (pos('Password:', s) <> 0) then
    SendCommand(edKSTpassw.Text);
  if (Logged = 1) AND (pos('Your choice           :', s) <> 0) then
  begin
    SendCommand(IntToStr(rgChat.ItemIndex + 1));
    Logged := 2;
    Timer2.Enabled := true;
  end;
m1: // ���������� �����
  Memo2.Lines.Add(' ');
  Start := 1;
  Stop := pos(CR, s);
  if Stop = 0 then
    Stop := length(s) + 1;
  while Start <= length(s) do
  begin
    Memo2.Lines.Strings[Memo2.Lines.Count - 1] := Memo2.Lines.Strings
      [Memo2.Lines.Count - 1] + copy(s, Start, Stop - Start);
    if s[Stop] = CR then
    begin
      Memo2.Lines.Add('');
    end;
    Start := Stop + 1;
    if Start > length(s) then
      Break;
    if s[Start] = LF then
      Start := Start + 1;
    Stop := Start;
    while (s[Stop] <> CR) and (Stop <= length(s)) do
      Stop := Stop + 1;
  end;
  if Logged = 3 then
  begin
    if pos('chat>', s) = 0 then
    begin
      ans := s;
      sleep(1000);
      exit;
    end
    else
    begin // ������������ ������ �� ������� ������
      if ans <> '' then
        ans := ans + s
      else
        ans := s;
      Logged := 2;
    end;
    UserList
  end;
end;

procedure TForm1.ReadSquareList;
var
  t: textfile;
  s: string;
begin
  if rgChat.ItemIndex = 0 then
  begin
    if (NOT FileExists('6M Work.txt')) OR (NOT FileExists('6M Confirm.txt'))
    then
      exit;
    assignfile(t, '6M Work.txt');
    reset(t);
    wkd := TStringList.Create;
    while not Eof(t) do
    begin // ������������ ������ ����������� ��������� 50���
      readln(t, s);
      if s[1] = '#' then
        continue;
      wkd.Add(s);
    end;
    closefile(t);
    assignfile(t, '6M Confirm.txt');
    reset(t);
    cfm := TStringList.Create;
    while not Eof(t) do
    begin // ������������ ������ ������������� ��������� 50���
      readln(t, s);
      if s[1] = '#' then
        continue;
      cfm.Add(s);
    end;
    closefile(t);
  end;
  if rgChat.ItemIndex = 1 then
  begin
    if (NOT FileExists('2M Work.txt')) OR (NOT FileExists('2M Confirm.txt'))
    then
      exit;
    assignfile(t, '2M Work.txt');
    reset(t);
    wkd := TStringList.Create;
    while not Eof(t) do
    begin // ������������ ������ ����������� ��������� 144���
      readln(t, s);
      if s[1] = '#' then
        continue;
      wkd.Add(s);
    end;
    closefile(t);
    assignfile(t, '2M Confirm.txt');
    reset(t);
    cfm := TStringList.Create;
    while not Eof(t) do
    begin // ������������ ������ ������������� ��������� 144���
      readln(t, s);
      if s[1] = '#' then
        continue;
      cfm.Add(s);
    end;
    closefile(t);
  end;
end;

procedure TForm1.rgChatClick(Sender: TObject);
begin
  ReadSquareList;
end;

procedure TForm1.UserList;
var
  s, ELoc: string;
  Start, Stop: Integer;
  Dist, Az, Elev, dAz: extended;
begin // ����� ������ ������ ����
  s := ans;
  ans := '';
  StringGrid2.RowCount := StringGrid2.RowCount + 1;
  Start := 1;
  Stop := pos(CR, s);
  if Stop = 0 then
    Stop := length(s) + 1;
  while Start <= length(s) do
  begin
    ans := copy(s, Start, Stop - Start); // ���������
    StringGrid2.Cells[0, StringGrid2.RowCount - 2] :=
      copy(ans, 1, pos(' ', ans) - 1); // call
    ans := copy(ans, pos(' ', ans), length(ans));
    ans := trimleft(ans);
    StringGrid2.Cells[1, StringGrid2.RowCount - 2] :=
      copy(ans, 1, pos(' ', ans) - 1); // loc
    ELoc := StringGrid2.Cells[1, StringGrid2.RowCount - 2];
    ans := copy(ans, pos(' ', ans), length(ans));
    ans := trimleft(ans);
    StringGrid2.Cells[2, StringGrid2.RowCount - 2] := copy(ans, 1, length(ans));
    // name
    MSDistAzim(Loc, ELoc, Dist, Az, Elev, dAz);
    StringGrid2.Cells[3, StringGrid2.RowCount - 2] := FloatToStr(Round(Az));
    StringGrid2.Cells[4, StringGrid2.RowCount - 2] := FloatToStr(Round(Dist));
    if s[Stop] = CR then
      StringGrid2.RowCount := StringGrid2.RowCount + 1;
    Start := Stop + 1;
    if Start > length(s) then
      Break;
    if s[Start] = LF then
      Start := Start + 1;
    Stop := Start;
    while (s[Stop] <> CR) and (Stop <= length(s)) do
      Stop := Stop + 1;
  end;
  StringGrid2.RowCount := StringGrid2.RowCount - 3;
end;

procedure TForm1.SendCommand(CommStr: string);
var
  i: Integer;
begin // �������� ������� ������� ����
  i := 1;
  while i <= length(CommStr) do
  begin
    IdTelnet1.SendCh(CommStr[i]);
    i := i + 1;
  end;
  IdTelnet1.SendCh(#13);
end;

procedure TForm1.IdTelnet1Disconnected(Sender: TObject);
begin
  btnConn.Caption := 'Connect';
end;

procedure TForm1.Intensity;
var
  Rel: integer;
begin // ����������� ������������� ������

  if SameDateTime(TMCDate,LDate) then
    begin
      pbIntens.Position := 0;
      exit;
    end;
  Rel:= CompareDateTime(TMCDate, MDate);
  if Rel=-1 then
  begin
    pbIntens.Position := Round(HoursBetween(TMCDate, FDate) * pbIntens.Max /
      HoursBetween(FDate, MDate));
  end;
  if Rel=1 then
  begin
    pbIntens.Position := Round(HoursBetween(TMCDate, LDate) * pbIntens.Max /
      HoursBetween(LDate, MDate));
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  ZoneInfo: TTimeZoneInformation;
  f: textfile;
begin
  // check files
  Path := ExtractFilePath(Application.ExeName);
  if NOT FileExists(Path + 'MSOrg.cfg') then
  begin
    ShowMessage('MSOrg.cfg file not found');
    Application.Terminate;
  end;
  if NOT FileExists(Path + 'MSOrg.db3') then
  begin
    ShowMessage('MSOrg.db3 file not found');
    Application.Terminate;
  end;
  if NOT FileExists(Path + '6M Confirm.txt') then
  begin
    assignfile(f, '6M Confirm.txt');
    rewrite(f);
    closefile(f);
  end;
  if NOT FileExists(Path + '2M Confirm.txt') then
  begin
    assignfile(f, '2M Confirm.TXT');
    rewrite(f);
    closefile(f);
  end;
  if NOT FileExists(Path + '6M Work.txt') then
  begin
    assignfile(f, '6M Work.txt');
    rewrite(f);
    closefile(f);
  end;
  if NOT FileExists(Path + '2M Work.txt') then
  begin
    assignfile(f, '2M Work.txt');
    rewrite(f);
    closefile(f);
  end;

  GetTimeZoneInformation(ZoneInfo);
  Bias := ZoneInfo.Bias / 60;
  if ( GetTimeZoneInformation(ZoneInfo) = TIME_ZONE_ID_DAYLIGHT ) then
    DaylightBias := ZoneInfo.DaylightBias / 60
  else DaylightBias := ZoneInfo.StandardBias / 60;
  Timer1Timer(Self);  // Set current date & time
  IniFile := TIniFile.Create(Path + 'MSOrg.cfg');
  Top := IniFile.ReadInteger('Form', 'FormTop', 0);
  Left := IniFile.ReadInteger('Form', 'FormLeft', 0);
  Height := IniFile.ReadInteger('Form', 'FormHeight', 300);
  Width := IniFile.ReadInteger('Form', 'FormWidth', 301);
  StringGrid1.ColWidths[0] := IniFile.ReadInteger('Form', 'Col0', 64);
  StringGrid1.ColWidths[1] := IniFile.ReadInteger('Form', 'Col1', 45);
  StringGrid1.ColWidths[2] := IniFile.ReadInteger('Form', 'Col2', 64);
  StringGrid1.ColWidths[3] := IniFile.ReadInteger('Form', 'Col3', 40);
  StringGrid1.ColWidths[4] := IniFile.ReadInteger('Form', 'Col4', 161);
  StringGrid1.Cells[0, 0] := 'Call';
  StringGrid1.Cells[1, 0] := 'Loc';
  StringGrid1.Cells[2, 0] := 'Date';
  StringGrid1.Cells[3, 0] := 'Time';
  StringGrid1.Cells[4, 0] := 'Notes';
  StringGrid1.DefaultRowHeight := 18;
  StringGrid1.Col := 0;
  StringGrid1.Row := 1;
  StringGrid2.ColWidths[0] := IniFile.ReadInteger('Form', '2Col0', 70);
  StringGrid2.ColWidths[1] := IniFile.ReadInteger('Form', '2Col1', 64);
  StringGrid2.ColWidths[2] := IniFile.ReadInteger('Form', '2Col2', 109);
  StringGrid2.ColWidths[3] := IniFile.ReadInteger('Form', '2Col3', 40);
  StringGrid2.ColWidths[4] := IniFile.ReadInteger('Form', '2Col4', 80);
  StringGrid2.Cells[0, 0] := 'Call';
  StringGrid2.Cells[1, 0] := 'Loc';
  StringGrid2.Cells[2, 0] := 'Name';
  StringGrid2.Cells[3, 0] := 'QTF';
  StringGrid2.Cells[4, 0] := 'QRB';
  StringGrid3.ColWidths[0] := IniFile.ReadInteger('Form', '3Col0', 20);
  StringGrid3.ColWidths[1] := IniFile.ReadInteger('Form', '3Col1', 100);
  StringGrid3.ColWidths[2] := IniFile.ReadInteger('Form', '3Col2', 70);
  StringGrid3.ColWidths[3] := IniFile.ReadInteger('Form', '3Col3', 70);
  StringGrid3.ColWidths[4] := IniFile.ReadInteger('Form', '3Col4', 70);
  StringGrid3.Cells[0, 0] := 'Abr';
  StringGrid3.Cells[1, 0] := 'Name';
  StringGrid3.Cells[2, 0] := 'Begin';
  StringGrid3.Cells[3, 0] := 'End';
  StringGrid3.Cells[4, 0] := 'Max';
  StringGrid3.Cells[5, 0] := 'ZHR';
  StringGrid3.Cells[6, 0] := 'V';
  StringGrid3.Cells[7, 0] := 'Lamb';
  StringGrid3.Cells[8, 0] := 'r';
  cbUsKST.Checked := IniFile.ReadBool('Conf', 'KST', false);
  edKSTpassw.Text := IniFile.ReadString('Conf', 'KSTpassw', '');
  DateTimePicker1.Height := 18;
  cmbSNTP.Items.Append(IniFile.ReadString('Conf', 'SNTP1', 'ntp.time.in.ua'));
  cmbSNTP.Items.Append(IniFile.ReadString('Conf', 'SNTP2', 'ntp2.time.in.ua'));
  cmbSNTP.Items.Append(IniFile.ReadString('Conf', 'SNTP3', 'ntp3.time.in.ua'));
  cmbSNTP.ItemIndex := IniFile.ReadInteger('Conf', 'SERV', 0);
  cbTSync.Checked := IniFile.ReadBool('Conf', 'Tsync', false);
  cmbPerSync.ItemIndex := IniFile.ReadInteger('Conf', 'PSYNC', 2);
  TimeSync;
  edQTFdelta.Text := IniFile.ReadString('Conf', 'QTFdelta', '20');
  edQRBmin.Text := IniFile.ReadString('Conf', 'QRBmin', '600');
  edQRBmax.Text := IniFile.ReadString('Conf', 'QRBmax', '2300');
  LoadNotes;
  Loc := IniFile.ReadString('Conf', 'Loc', 'kn69pc');
  edHSLoc.Text := Loc;
  edHLoc.Text := Loc;
  lbLocator.Caption := Loc;
  Call := IniFile.ReadString('Conf', 'Call', 'UT2VR');
  edCall.Text := Call;
  lbStation.Caption := Call;
  rgAzIndic.ItemIndex := IniFile.ReadInteger('Conf', 'AzIndic', 0);
  rgChat.ItemIndex := IniFile.ReadInteger('Conf', 'Chat', 0);
  FDConnection1.Params.Database := Path + 'MSOrg.db3';
//  if cbUsKST.Checked then
//    btnConnClick(Self);
  ReadSquareList;
  ReadShowers;
  DrawSky;
  Answ := TStringList.Create;
  if CheckLocator(edHSLoc.Text) then
  begin
    Coordinate(Loc, Latitude, Longitude);
    PageControl1.ActivePageIndex := 0; // Shower info
  end
  else
  begin
    PageControl1.ActivePageIndex := 4; // Setup
  end;
  try
    with LoTWUserQuery do
    begin // Load LoTW user
      SQL.Clear;
      SQL.Text := 'select Call from tblLoTW_User';
      Open;
    end;
  except
    on E: EDatabaseError do
    begin
      ShowMessage(E.Message);
      Application.Terminate;
    end;
  end;
end;

procedure �orrectYear(Year: integer);
var
  i: Integer;
  s: string;
begin
    try
      with Form1.FDQuery1 do
        begin
          SQL.Clear;
          SQL.Text:= 'update tblShowersInfo set StartDate=date(StartDate, '+
            chr(39)+IntToStr(Year)+' year'+chr(39)+')';
          ExecSQL;
          SQL.Clear;
          SQL.Text:= 'update tblShowersInfo set EndDate=date(EndDate, '+
            chr(39)+IntToStr(Year)+' year'+chr(39)+')';
          ExecSQL;
          SQL.Clear;
          SQL.Text:= 'update tblShowersInfo set MaxDate=date(MaxDate, '+
            chr(39)+IntToStr(Year)+' year'+chr(39)+')';
          ExecSQL;
        end;
    except
    on E: Exception do
      begin
        ShowMessage('Update tblShowersInfo ' + E.Message);
        Exit;
      end;
    end;
    try
      with Form1.FDQuery1 do
        begin
          SQL.Clear;
          SQL.Text:= 'update tblShowersData set Date=date(Date, '+
            chr(39)+IntToStr(Year)+' year'+chr(39)+')';
          ExecSQL;
        end;
    except
    on E: Exception do
      begin
        ShowMessage('Update tblShowersInfo ' + E.Message);
        Exit;
      end;
    end;
end;

procedure TForm1.ReadShowers;
var
  i, SelRow: Integer;
  s, Name, TxtCurDate,q: string;
  CreationYear, UseYear: Word;   // Calendar update year
begin
// ��������� ���� ������������ ���������
    try
      with FDQuery1 do
        begin
          SQL.Clear;
          SQL.Text:= 'select BaseTable, UpdDate from tblUpdDate where BaseTable='+
              chr(39)+'Showers'+chr(39);
          Open;
          CreationYear:= YearOf(FieldByName('UpdDate').AsDateTime);
          SQL.Clear;
          SQL.Text:= 'select StartDate from tblShowersInfo where ShortName='+
              chr(39)+'ACE'+chr(39);
          Open;
          UseYear:= YearOf(FieldByName('StartDate').AsDateTime);
        end;
    except
    on E: Exception do
      begin
        ShowMessage('Read tblUpdDate ' + E.Message);
        Exit;
      end;
    end;
  if CreationYear<CYear then           ShowMessage('Showers calendar contains previous year information.'+
            #13#10+ 'Update required.');
  if UseYear<>CYear then
        begin     // ��������� �� ��������� ���, ��������� ���������
          ShowMessage('Showers calendar contains outdated information.'+
            #13#10+ 'Correction required.');
            i:= CYear-UseYear;
            �orrectYear(i);
        end;
// ���������� Items  StringGrid3, cbActiveShowers
    try
      with FDQuery1 do
        begin
          SQL.Clear;
          SQL.Text:= 'select FullName, ShortName, StartDate, EndDate, MaxDate,'
             + 'ZHR, V, r, Lambda from tblShowersInfo order by MaxDate';
          Open;
        end;
    except
    on E: Exception do
      begin
        ShowMessage('Read tblShowersInfo ' + E.Message);
        Exit;
      end;
    end;
  i:= 1;
  while NOT FDQuery1.Eof do
    begin
      StringGrid3.Cells[0,i]:= FDQuery1.FieldByName('ShortName').AsString;
      StringGrid3.Cells[1,i]:= FDQuery1.FieldByName('FullName').AsWideString;
      StringGrid3.Cells[2,i]:= FDQuery1.FieldByName('StartDate').AsString;
      StringGrid3.Cells[3,i]:= FDQuery1.FieldByName('EndDate').AsString;
      StringGrid3.Cells[4,i]:= FDQuery1.FieldByName('MaxDate').AsString;
      StringGrid3.Cells[5,i]:= FDQuery1.FieldByName('ZHR').AsString;
      StringGrid3.Cells[6,i]:= FDQuery1.FieldByName('V').AsString;
      StringGrid3.Cells[7,i]:= FDQuery1.FieldByName('Lambda').AsString;
      StringGrid3.Cells[8,i]:= FDQuery1.FieldByName('r').AsString;
      FDQuery1.Next;
      StringGrid3.RowCount:= StringGrid3.RowCount+1;
      i:= i+1;
    end;
  StringGrid3.RowCount:= StringGrid3.RowCount-1;
// ���������� Items  cbActiveShowers
  TxtCurDate:= FormatDate(CUTCDate, true);
  delete(TxtCurDate,12,9);
  q:= 'select FullName, ShortName from tblShowersInfo where '+TxtCurDate+
    ' BETWEEN StartDate AND EndDate';
    try
      with FDQuery1 do
        begin
          SQL.Clear;
          SQL.Text:= q;
          Open;
        end;
    except
    on E: Exception do
      begin
        ShowMessage('Read tblShowersInfo ' + E.Message);
        Exit;
      end;
    end;
    Name:= '';
    while NOT FDQuery1.Eof do
    begin
      s:= FDQuery1.FieldByName('FullName').AsWideString;
      Name:=  FDQuery1.FieldByName('ShortName').AsString;
      cbActiveShowers.Items.Append(s);
      FDQuery1.Next;
    end;
  cbActiveShowers.ItemIndex:= 0;
// ���������� �� ������ �������� �����
  SelRow:= GridSerchInCol(StringGrid3, 0, Name);
  if SelRow<>-1 then GridSellectRow(StringGrid3, SelRow);
end;

procedure TForm1.StringGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  R: TRect;
  Time: TTime;
begin
  ACol := StringGrid1.Col;
  ARow := StringGrid1.Row;
  if Key = 13 then
  begin // ��������� ������ ����� � ������� �� Enter
    StatusBar1.Panels[0].Text := '';
    StringGrid1.Options := StringGrid1.Options - [goEditing];
    R := StringGrid1.CellRect(ACol, ARow);
    R.Left := R.Left + StringGrid1.Left;
    R.Right := R.Right + StringGrid1.Left;
    R.Top := R.Top + StringGrid1.Top;
    R.Bottom := R.Bottom + StringGrid1.Top;
    case ACol of
      0: // Call
        begin
          meCall.Text := StringGrid1.Cells[ACol, ARow];
          meCall.Left := R.Left + 1;
          meCall.Top := R.Top + 1;
          meCall.Width := (R.Right + 1) - R.Left;
          meCall.Height := (R.Bottom + 1) - R.Top;
          meCall.Visible := true;
          meCall.SetFocus;
          StringGrid1.Enabled := false;
          StatusBar1.Panels[0].Text := 'Write callsign & press Enter';
        end;
      1: // Locator
        begin
          meLoc.Text := StringGrid1.Cells[ACol, ARow];
          meLoc.Left := R.Left + 1;
          meLoc.Top := R.Top + 1;
          meLoc.Width := (R.Right + 1) - R.Left;
          meLoc.Height := (R.Bottom + 1) - R.Top;
          meLoc.Visible := true;
          meLoc.SetFocus;
          StringGrid1.Enabled := false;
          StatusBar1.Panels[0].Text := 'Write QRA locator & press Enter';
        end;
      2: // Date
        begin
          if StringGrid1.Cells[ACol, ARow] <> '' then
          begin
            try
              DateTimePicker1.Date := StrToDate(StringGrid1.Cells[ACol, ARow])
            except
              DateTimePicker1.Date := Now;
            end;
          end
          else
            DateTimePicker1.Date := Now;
          DateTimePicker1.Left := R.Left + 1;
          DateTimePicker1.Top := R.Top + 1;
          DateTimePicker1.Width := (R.Right + 1) - R.Left;
          DateTimePicker1.Height := (R.Bottom + 1) - R.Top;
          DateTimePicker1.Visible := true;
          DateTimePicker1.SetFocus;
          StringGrid1.Enabled := false;
          StatusBar1.Panels[0].Text := 'Select date & press Enter';
        end;
      3: // Time
        begin
          if StringGrid1.Cells[ACol, ARow] <> '' then
          begin
            meTime.Text := StringGrid1.Cells[ACol, ARow];
            try
              Time := StrToTime(meTime.Text);
            except
              meTime.Text := '';
            end;
          end
          else
            meTime.Text := copy(TimeToStr(LocalTimeToUTC(Now)), 1, 5);
          meTime.Left := R.Left + 1;
          meTime.Top := R.Top + 1;
          meTime.Width := (R.Right + 1) - R.Left;
          meTime.Height := (R.Bottom + 1) - R.Top;
          meTime.Visible := true;
          meTime.SetFocus;
          StringGrid1.Enabled := false;
          StatusBar1.Panels[0].Text := 'Write time & press Enter';
        end;
      4: // notes
        begin
          StringGrid1.Options := StringGrid1.Options + [goEditing];
          StatusBar1.Panels[0].Text := 'Write notes & press Enter';
        end;
    end;
    if (ARow = StringGrid1.RowCount - 1) AND
      (StringGrid1.Cells[0, StringGrid1.RowCount - 1] <> '') then
      StringGrid1.RowCount := StringGrid1.RowCount + 1;
  end;
end;

procedure TForm1.StringGrid1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  with StringGrid1 do
    try
      MouseToCell(X, Y, ACol, ARow); // ����������� ������ ��� ��������
      if (ACol = 4) AND (length(Cells[ACol, ARow]) > 35) then
        Hint := Cells[ACol, ARow]
      else
        Hint := '';
    except
    end;
end;

procedure TForm1.StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: boolean);
begin // ��������� ������ ������ �������
  StatusBar1.Panels[0].Text := '';
  if (PageControl1.ActivePageIndex <> 2) OR (ARow = 0) then
    exit; // Work
  if (length(StringGrid1.Cells[1, ARow]) = 6) then
  begin // ���� � �������� ������ ���� ��� �� ��������� ���
    edKLoc.Text := StringGrid1.Cells[1, ARow];
    ViewAzDist;
  end
  else
  begin
    edKLoc.Text := '';
    edDist.Text := '';
    edAzim.Text := '';
    edElev.Text := '';
  end;
end;

procedure TForm1.StringGridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  index: Integer;
  s: string;
  LoTW_User: boolean;
label CheckLoTW;
begin
  with (Sender as TStringGrid) do
  begin
    if (ACol = 1) AND (ARow <> 0) AND (Cells[ACol, ARow] <> '') then
    begin // ������������� ��������� ��� ��������� �����
      s := Cells[ACol, ARow];
      if (cfm.Find(copy(s, 1, 4), index)) then
      begin // cfm
        Canvas.Brush.Color := clWhite;
        Canvas.Font.Color := clBlack;
        Canvas.TextRect(Rect, Rect.Left + 3, Rect.Top + 3, s);
        goto CheckLoTW;
      end;
      if (wkd.Find(copy(s, 1, 4), index)) then
      begin // wkd & not cfm
        Canvas.Brush.Color := clWhite;
        Canvas.Font.Color := clBlue;
        Canvas.TextRect(Rect, Rect.Left + 3, Rect.Top + 3, s);
        goto CheckLoTW;
      end;
      // not wkd & not cfm
      Canvas.Brush.Color := clWhite;
      Canvas.Font.Color := clRed;
      Canvas.TextRect(Rect, Rect.Left + 3, Rect.Top + 3, s);
    end;
  CheckLoTW: // ������������� �������� ��� ��������� �����
    if (ACol = 0) AND (ARow <> 0) AND (Cells[ACol, ARow] <> '') then
    begin
      s := Cells[ACol, ARow];
      try
        if LoTWUserQuery.Locate('Call', s, []) then
          LoTW_User := true
        else
          LoTW_User := false;
      except
        on E: Exception do
          // StatusBar1.Panels.Items[2].Text:= 'LoTWUserQuery ' + E.Message;
          ShowMessage('FindInfo Locate' + E.Message);
      end;
      if LoTW_User then
      begin // LoTW_User
        Canvas.Brush.Color := clWhite;
        Canvas.Font.Color := clGreen;
        Canvas.TextRect(Rect, Rect.Left + 3, Rect.Top + 3, s);
      end;
    end;
  end;
end;

procedure TForm1.StringGrid2MouseActivate(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y, HitTest: Integer;
 var MouseActivate: TMouseActivate);
begin // ���������� �� ��������� �������
  StringGrid2.MouseToCell(X, Y, ACol, ARow);
  if (Button = mbLeft) AND (ARow = 0) then
    case ACol of
      0:
        GridSort(StringGrid2, ACol);
      1:
        GridSort(StringGrid2, ACol);
      2:
        exit;
      3:
        GridSortNum(StringGrid2, ACol);
      4:
        GridSortNum(StringGrid2, ACol);
    end;
  if ((Button = mbLeft) AND (ARow <> 0)) AND (ssCtrl in Shift) then
  begin // ����������� ������ � ������ �������
    PageControl1.TabIndex := 2; // Tab Work
    StringGrid1.SetFocus;
    if StringGrid1.Cells[0, StringGrid1.RowCount - 1] <> '' then
      StringGrid1.RowCount := StringGrid1.RowCount + 1;
    StringGrid1.Cells[0, StringGrid1.RowCount - 1] := StringGrid2.Cells
      [0, ARow];
    StringGrid1.Cells[1, StringGrid1.RowCount - 1] := StringGrid2.Cells
      [1, ARow];
    StringGrid1.Row := StringGrid1.RowCount - 1;
    StringGrid1.Col := 0;
  end;
end;

procedure TForm1.StringGrid2MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin // ����� ���������
  with StringGrid2 do
    try
      MouseToCell(X, Y, ACol, ARow);
      if ARow = 0 then
        case ACol of
          0:
            Hint := 'Sort by Call';
          1:
            Hint := 'Sort by Loc';
          2:
            Hint := '';
          3:
            Hint := 'Sort by QTF';
          4:
            Hint := 'Sort by QRB';
        end;
      if (ARow <> 0) AND (ACol <= 1) then
        Hint := 'Press Ctrl & Click to copy';
    except
    end;
end;

procedure TForm1.DateTimePicker1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin // ���� ����  � ������
  If Key = 13 then
  begin
    StringGrid1.Cells[StringGrid1.Col, StringGrid1.Row] :=
      DateToStr(DateTimePicker1.Date);
    DateTimePicker1.Visible := false;
    StringGrid1.Enabled := true;
    StringGrid1.SetFocus;
    StatusBar1.Panels[0].Text := '';
  end;
end;

procedure TForm1.StringGrid3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
//Label28.Caption:= IntToStr(StringGrid3.Col);
//Label29.Caption:= IntToStr(StringGrid3.Row);
end;

procedure TForm1.StringGrid3MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  with StringGrid3 do
    try
      MouseToCell(X, Y, ACol, ARow); // ����������� ������ ��� ��������
      Hint := 'ZHR='+Cells[6, ARow]+'  V='+ Cells[7, ARow];
    except
      Hint := '';
    end;
end;

procedure TForm1.StringGrid3SelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var
  Lambda, JD: extended;
  Hour, Min, Sec: extended;
  Day: integer;
begin
  lbShow.Caption := StringGrid3.Cells[1,ARow];
  STMName:= StringGrid3.Cells[0,ARow];
  FDate :=  StrToDateTime(StringGrid3.Cells[2,ARow]);
  LDate:= StrToDateTime(StringGrid3.Cells[3,ARow]);
  MDate:= StrToDateTime(StringGrid3.Cells[4,ARow]);
  tbDate.Max := DaysBetween(LDate, FDate);
  if StringGrid3.Cells[7,ARow]<>'' then
  begin
    Lambda:= StrToFloat(StringGrid3.Cells[7,ARow]);
    JD:= SolarLongToJD(Lambda);
    JDCD(JD, GDay, GMonth, GYear);
    Day:= trunc(GDay);
    DHHMS((GDay-Day)*24, Hour,Min,Sec);
    lbMax.Caption:= IntToStr(Day)+'.'+FloatToStr(GMonth)+'.'+FloatToStr(GYear)+' '+
    FloatToStr(Hour)+':'+FloatToStr(Min);
  end
  else  lbMax.Caption:= '';
  lbStart.Caption := DateToStr(FDate);
  lbEnd.Caption := DateToStr(LDate);
  tbDate.Position:=0;
  tbTime.Position:=0;
end;

procedure TForm1.SelectCondition;
var
  QTF, QTF1, QTF2, QTF1min, QTF1max, QTF2min, QTF2max, i: Integer;
  QRB, Min, Max, delta: Integer;
  Condition: boolean;

label m1;
begin // �������� �� ������� ���, ��� ������������� ��������
  if (edAzimuth1.Text = '') then
    exit;
  if (NOT cbSelect.Checked) then
  begin
    btnUserClick(Self);
    exit;
  end;
  delta := StrToInt(edQTFdelta.Text);
  Min := StrToInt(edQRBmin.Text);
  Max := StrToInt(edQRBmax.Text);
  QTF1 := Round(StrToFloat(edAzimuth1.Text));
  QTF2 := QTF1 + 180;
  if QTF2 > 360 then
    QTF2 := QTF2 - 360;
  QTF1min := QTF1 - delta;
  if QTF1min < 0 then
    QTF1min := QTF1min + 360;
  QTF2min := QTF2 - delta;
  if QTF2min < 0 then
    QTF2min := QTF1min + 360;
  QTF1max := QTF1 + delta;
  if QTF1max > 360 then
    QTF1max := QTF1max - 360;
  QTF2max := QTF2 + delta;
  if QTF2max > 360 then
    QTF2max := QTF2max - 360;
  GridSortNum(StringGrid2, 3);
m1:
  for i := 1 to StringGrid2.RowCount - 1 do
  begin
    if StringGrid2.Cells[3, i] = '' then
      Break;
    QTF := StrToInt(StringGrid2.Cells[3, i]);
    QRB := StrToInt(StringGrid2.Cells[4, i]);
    Condition := (((QTF > QTF1min) AND (QTF < QTF1max)) OR
      ((QTF > QTF2min) AND (QTF < QTF2max)));
    Condition := Condition AND ((QRB > Min) AND (QRB < Max));
    if NOT Condition then
    begin
      DeleteARow(StringGrid2, i);
      goto m1;
    end;
  end;
end;

procedure TForm1.cbSelectMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  SelectCondition;
end;

procedure TForm1.GetShowerPar(SName: string; Date: TDateTime);
var
  i: integer;
begin
  with FDQuery1 do   // ��������� ���������� ������
    begin
      SQL.Clear;
      SQL.Text:= 'select RA, Dec, Date from '+
        'tblShowersData where ShortName='+chr(39)+SName+chr(39);
      Open;
    end;
    i:=1;
    while i <= FDQuery1.RecordCount do
      begin     // ����� ��������� ����
        if WithinPastDays(DateOf(Date), FDQuery1.FieldByName('Date').AsDateTime, 2) then
          begin
            RA:= FDQuery1.FieldByName('RA').AsFloat;
            Decl:= FDQuery1.FieldByName('Dec').AsFloat;
          end;
        i:= i+1;
        FDQuery1.Next;
      end;
end;

procedure TForm1.cbActiveShowersSelect(Sender: TObject);
var
  q: string;
begin
  // ��� ��������� ��������� ��� � ��������
  q := cbActiveShowers.Items[cbActiveShowers.ItemIndex];
  with FDQuery1 do   // ����� ��������� �����
    begin
      SQL.Clear;
      SQL.Text:= 'select ShortName, ZHR, V, Lambda from '+
        'tblShowersInfo where FullName='+chr(39)+q+chr(39);
      Open;
      SName:= FieldByName('ShortName').AsString;
      lbZHR.Caption:= FieldByName('ZHR').AsString+'  per hour';
      lbV.Caption:= FieldByName('V').AsString+'  km/s';

    end;
  GetShowerPar(SName, CUTCDate);
  ShowerCalc(RA, Decl, CLCTDate, Az, Alt);
  DrawSky;
  Radiant(Sname);
end;

procedure TForm1.cbTSyncClick(Sender: TObject);
begin
  if cbTSync.Checked then
    TimeSync;
end;

procedure TForm1.Radiant(Sname: string);
var
  z, Azim: extended;
  X, Y: Integer;
  Col: Integer;
  s: string;
begin // ��������� ��������� ��������
  if (RA = 0) AND (Decl = 0) then
    exit;
  if rgAzIndic.ItemIndex = 0 then
    Azim := Az + 90
  else
    Azim := Az;
  if Azim > 360 then
    Azim := Azim - 360;
  if Alt >= 0 then
  begin
    // ��������� ��������
    Image1.Canvas.Brush.Color := clRed;
    Image1.Canvas.MoveTo(130, 130);
    z := R / 2 * cos(DegToRad(Alt));
    X := Round(R / 2 + z * sin(DegToRad(Az)));
    Y := Round(R / 2 - z * cos(DegToRad(Az)));
    Image1.Canvas.Ellipse(X - 4, Y - 4, X + 4, Y + 4);
    // �������� �����
    Image1.Canvas.Brush.Color := clGreen; // $F0CAA6;
    Image1.Canvas.Pen.Color := clGreen;
    X := Round(R / 2 + R / 2 * sin(DegToRad(Azim)));
    Y := Round(R / 2 - R / 2 * cos(DegToRad(Azim)));
    Image1.Canvas.MoveTo(X, Y);
    Azim := Azim + 180;
    if Azim > 360 then
      Azim := Azim - 360;
    X := Round(R / 2 + R / 2 * sin(DegToRad(Azim)));
    Y := Round(R / 2 - R / 2 * cos(DegToRad(Azim)));
    Image1.Canvas.LineTo(X, Y);
  end;
  if Alt <= 0 then
    Col := clRed
  else
    Col := clLime;
  edAltitude.Color := Col;
  edAzimuth1.Color := Col;
  edAzimuth2.Color := Col;
  edAltitude.Text := FormatFloat('0.0', Alt);
  if Azim > 360 then
    Azim := Azim - 360;
  edAzimuth1.Text := FormatFloat('0.0', Azim);
  s := FormatFloat('###', Azim) + '<-' + SName + '->';
  Azim := Azim + 180;
  if Azim > 360 then
    Azim := Azim - 360;
  edAzimuth2.Text := FormatFloat('0.0', Azim);
  s := s + FormatFloat('###', Azim) + ' ';
  s := s + 'Alt=' + FormatFloat('###', Alt);
  TrayIcon1.Hint := s;
  // �����
  Image1.Canvas.Brush.Color := clBtnFace;
  Image1.Canvas.Pen.Color := clBtnFace;
  Image1.Canvas.Pen.Color := clRed;
  Image1.Canvas.TextOut(22, 22, SName);
end;

procedure TForm1.DrawSky;
var
  i: Integer;
  X, Y: Integer;
  z: extended;
begin
  X := Round(R);
  i := 1;
  Image1.Canvas.Brush.Color := clBtnFace;
  Image1.Canvas.Pen.Color := clBtnFace;
  Image1.Canvas.Rectangle(0, 0, 260, 260);
  Image1.Canvas.Brush.Color := $F0CAA6;
  Image1.Canvas.Pen.Color := clBlack;
  Image1.Canvas.Ellipse(0, 0, X, X); // ������� ����
  Image1.Canvas.MoveTo(0, Round(X / 2));
  Image1.Canvas.LineTo(X, Round(X / 2));
  Image1.Canvas.MoveTo(Round(X / 2), 0);
  Image1.Canvas.LineTo(Round(X / 2), X);
  // ��������
  while i <= 5 do
  begin
    z := R * cos(DegToRad(i * 15));
    X := Round(R / 2 - z / 2);
    Y := Round(R / 2 + z / 2);
    Image1.Canvas.Brush.Style := bsClear;
    Image1.Canvas.Ellipse(X, X, Y, Y);
    Image1.Canvas.Brush.Style := bsSolid;
    Image1.Canvas.Brush.Color := $F0CAA6;
    if odd(i) then
      Image1.Canvas.TextOut(Round(R / 2 - 6), X - 6, IntToStr(i * 15))
    else
      Image1.Canvas.TextOut(Round(R / 2 - 6), Y - 8, IntToStr(i * 15));
    i := i + 1;
  end;
  Image1.Canvas.Font.Color := clRed;
  Image1.Canvas.TextOut(0, Round(R / 2 - 6), 'W');
  Image1.Canvas.TextOut(Round(R - 10), Round(R / 2 - 6), 'E');
  // ����� ��������
  edAltitude.Color := clWhite;
  edAzimuth1.Color := clWhite;
  edAzimuth2.Color := clWhite;
  edAzimuth1.Text := '';
  edAzimuth2.Text := '';
  edAltitude.Text := '';
end;

procedure TForm1.edCallKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin // ���� ������ ���������
  If Key = 13 then
  begin
    Call := UpperCase(edCall.Text);
    edCall.Text := Call;
    lbStation.Caption := Call;
    StatusBar1.Panels[0].Text := '';
  end;
end;

procedure TForm1.edCallMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  StatusBar1.Panels[0].Text := 'Write callsign & press Enter';
end;

procedure TForm1.edCommandKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin // ���� ������� ��� ������� ����
  if Key = 13 then
  begin
    Memo2.Lines.Add(edCommand.Text);
    SendCommand(edCommand.Text);
    edCommand.Text := '';
  end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin // ����������� ��� �������������
  GetCurrDateTime(false, CLCTDate, CYear, CMonth, CDay, CHour, CMin, CSec, CMsec);
  CUTCDate:= LocalTimeToUTC(CLCTDate);
  lbClock.Caption := TimeToStr(CUTCDate);
  if PageControl1.ActivePageIndex = 2 then // Tab Work
  begin
    ShowerCalc(RA, Decl, CLCTDate, Az, Alt);
    DrawSky;
    Radiant(SName);
    //  ��� �������� �� WSJT
//    Label1.Caption:= FloatToStr(RA/15);
//    Label27.Caption:= FloatToStr(Decl);
  end;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin // ������ ������ ��������� ������ ������
  if (Logged <> 2) OR (cbFreeze.Checked) then
    exit;
  SendCommand('/SH US');
  Logged := 3;
  ans := '';
  ClearTable(StringGrid2);
  StringGrid2.RowCount := 2;
end;

procedure TForm1.Timer3Timer(Sender: TObject);
begin // ������� ����� ����� �������� ��������
  TimeSync;
end;

procedure TForm1.TrayIcon1Click(Sender: TObject);
begin // ��������� ����� �� ������ � ����
  Form1.Show;
  SendMessage(Form1.Handle, WM_SYSCOMMAND, SC_RESTORE, 0);
end;

procedure TForm1.WMSysCommand;
begin // ��� ����������� ������� � ����
  if Msg.CmdType = SC_MINIMIZE then
  begin
    PageControl1.TabIndex := 2;
    Form1.Hide;
    TrayIcon1.Visible := true;
  end;
  inherited; // ������������ ����� ��-���������
end;

end.

